<?php

class Animal
{
    private $name;
    private $legs;
    private $cold_blooded;

    public function __construct($name, $legs = 4, $cold_blooded = "no")
    {    
        $this->name = $name;
        $this->legs = $legs;
        $this->cold_blooded = $cold_blooded;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLegs()
    {
        return $this->legs;
    }
    public function getColdBlooded()
    {
        return $this->cold_blooded;
    }

    public function cetak()
    {
        echo "Name : ".$this->getName()."<br/>";
        echo "legs : ".$this->getLegs()."<br/>";
        echo "cold blooded : ".$this->getColdBlooded()."<br/><br/>";
    }
}

// class Ape extends Animal
// {
    
//     public function yell()
//     {
//         return "Auooo";
//     }
//     public function cetak()
//     {
//         echo "Name : ".$this->getName()."<br/>";
//         echo "legs : ".$this->getLegs()."<br/>";
//         echo "cold blooded : ".$this->getColdBlooded()."<br/>";
//         echo "Yell : ".$this->yell()."<br/><br/>";
//     }
// }

// class Frog extends Animal
// {
    
//     public function jump()
//     {
//         return "hop hop";
//     }
//     public function cetak()
//     {
//         echo "Name : ".$this->getName()."<br/>";
//         echo "legs : ".$this->getLegs()."<br/>";
//         echo "cold blooded : ".$this->getColdBlooded()."<br/>";
//         echo "Jump : ".$this->jump()."<br/><br/>";
//     }
// }

//$sheep = new Animal("shaun");
// echo $sheep->getName();
// echo $sheep->getLegs();
// echo $sheep->getColdBlooded();
//$sheep->cetak();

//$kodok = new Frog("buduk");
// echo $kodok->getName();
// echo $kodok->getLegs();
// echo $kodok->getColdBlooded();
// echo $kodok->jump() ; // "hop hop"
//$kodok->cetak();

//$sungokong = new Ape("kera sakti",2);
// echo $sungokong->getName();
// echo $sungokong->getLegs();
// echo $sungokong->getColdBlooded();
// echo $sungokong->yell();// "Auooo"
//$sungokong->cetak();
?>

